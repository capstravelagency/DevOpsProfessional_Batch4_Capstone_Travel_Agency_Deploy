
Deploy part involves in creation of Tomcat server through Ansbile ,deploy the artifact. This script works with the sudo user for not having to 
mess with the privilege issues and lot of ansible permission related issues to deal with it ,atleast temporarily.

#Ansible Installation 

Mint Cinamon OS Linux 
Version : Linux devi-VirtualBox 4.8.0-53-generic #56~16.04.1-Ubuntu SMP Tue May 16 01:18:56 UTC 2017 x86_64 x86_64 x86_64 GNU/Linux

# Install Ansible on Ubuntu 14.04

    https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-ansible-on-ubuntu-14-04



    sudo apt-get update
    sudo apt-get install software-properties-common
    sudo apt-add-repository ppa:ansible/ansible

    sudo apt-get update
    sudo apt-get install ansible

# Add below in ansible.cfg

    sudo_user      = root
    ask_sudo_pass = False
    become_user = sudo
    become = True
    become_method = sudo

# Add below in /etc/sudoers
    Note : type visudo  (Saving in Nano follow these - > ) Ctrl o and ENTER , Ctrl X

    # User privilege specification
    
    root    ALL=(ALL:ALL) ALL
    jenkins ALL=(ALL:ALL) ALL
    ansible ALL=(ALL:ALL) ALL

    jenkins ALL=(ALL) NOPASSWD: ALL
    ansible ALL=(ALL) NOPASSWD: ALL
    root    ALL=(ALL) NOPASSWD: ALL


